import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ItemListComponent } from './components/item-list.component/item-list.component';
import { EditListComponent } from './components/edit-list.component/edit-list.component';

const routes: Routes = [
  { path: '', redirectTo: '/items', pathMatch: 'full' },

  { path: 'items', component: ItemListComponent },
  { path: 'edit', component: EditListComponent },


];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {}
