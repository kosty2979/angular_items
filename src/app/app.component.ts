import { Component } from '@angular/core';

import {ItemsService} from './services/items.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  sort = false;
  constructor(private itemsService: ItemsService) {
  };

  public setSort(sort: boolean): void {
    this.sort = sort;
    this.itemsService.setSort(this.sort)
  };

  public getClass(sort: boolean): string {
    return sort === this.sort ? 'activeSortBtn' : ''
  }
}
