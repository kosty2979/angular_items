import {Injectable} from '@angular/core';

import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';

import {Item} from '../classes/item';
import {StartArray} from '../classes/StartArray';

@Injectable()
export class ItemsService {
  public items: Item [] = [];
  public sortItems: Item [] = [];
  private sort = false;
  private startArray = new StartArray();
  private itemsSubject = new Subject<Item []>();

  constructor() {
    this.setBaseItems()
  };

  private setBaseItems() {
    this.startArray.items.forEach((item, i) => {
      this.items.push(new Item(item.text, item.cart, i + 1))
    });
    this.itemsSorting()
  };

  public getItems(): Observable<Item []> {
    setTimeout(() => {
      this.itemsSubject.next(this.sortItems)
    }, 0);
    return this.itemsSubject.asObservable()
  };

  public changeCart(id: number): void {
    this.items.map(item => {
      if (item.id === id) {
        item.cart = !item.cart
      }
    });
    this.itemsSorting();
  };
  public changeItem(i: Item): void {
     this.items.map( item => {
      if (item.id === i.id) {
        item.cart = i.cart;
        item.text = i.text;
      }
    });
    this.itemsSorting();
  };

  public deleteItem(item: Item): void {
    this.items.splice(this.items.indexOf(item), 1);
    this.itemsSorting();
  };

  public createItem( text: string): void {
    this.items.push(new Item(text, false ));
    this.itemsSorting();
  };

  public setSort(sort: boolean): void {
    this.sort = sort;
    this.itemsSorting();
  };

  public itemsSorting(): void {
    this.sortItems = this.sort ? (this.items.filter(item => {
      return item.cart === true
    }) ) : this.items.slice();
    this.itemsSubject.next(this.sortItems);
  };
}

