export class StartArray {
  items: { text: string, cart: boolean }[];

  constructor() {
    this.items = [
      {text: 'Milk 1l', cart: false},
      {text: 'Eggs medium 12 pack', cart: true},
      {text: 'Fresh Basil', cart: false},
      {text: 'Wholegrain Bread 1 pkg', cart: true},
      {text: 'Ground Coffe 200g', cart: true},
      {text: 'Red Wine', cart: false},
      {text: 'Mozzarella Cheese 150g', cart: false},
      {text: 'Orange juice 1l', cart: true},
      {text: 'Tomatoes', cart: false}
    ];
  };
}
