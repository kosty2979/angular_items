export class Item {
  id: number;
  text: string;
  cart: boolean;

  constructor(text: string, cart: any, id?: number) {
    this.id =  id ? id : Date.now();
    this.text = text;
    this.cart = cart ;
  }
}
