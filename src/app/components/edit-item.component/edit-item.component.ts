import {Component, Input, Output, EventEmitter} from '@angular/core';

import { Item } from '../../classes/item'



@Component({
  selector: 'edit-item',
  templateUrl: './edit-item.component.html',
  styleUrls: ['./edit-item.component.css']
})

export class EditItemComponent {
  @Input() item: Item;
  @Output() remove = new EventEmitter();
  @Output() change = new EventEmitter();
  edit = false;

  public removeItem() {
    this.remove.emit( this.item )
  };
  public changeItem(event: any): void {
    const item: Item = Object.assign({}, this.item);
    if (event === 'cart') {
      item.cart = !item.cart;
      this.change.emit( item );
      return
    }
    this.edit = false;
    let text = event.target.value.trim();
    if ( text === this.item.text || text.length === 0 ) { return }
    if ( text.length > 30 ) { text = text.substring(0, 29)}
    item.text = text;
    this.change.emit( item )
  }

}
