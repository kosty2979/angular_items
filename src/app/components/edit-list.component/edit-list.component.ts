import {Component, OnInit, OnDestroy, ViewChild} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import {Item} from '../../classes/item';

import { Subscription } from 'rxjs/Subscription';

import {ItemsService} from '../../services/items.service'



@Component({
  selector: 'edit-list',
  templateUrl: './edit-list.component.html',
  styleUrls: ['./edit-list.component.css']
})

export class EditListComponent implements OnInit, OnDestroy  {
  @ViewChild('createModal') public periodModal: ModalDirective;
  items: Item[];
  newItem = '';
  subscription: Subscription;

  constructor(private itemsService: ItemsService) {
  };

  ngOnInit() {
    this.subscription = this.itemsService.getItems().subscribe(items => {
      this.items = items;
    });
  }

  public deleteItem(item: Item): void {
    this.itemsService.deleteItem(item)
  };

  public changeItem(item: Item): void {
    this.itemsService.changeItem(item)
  };

  public closeModal(): void {
    this.newItem = ''
    this.periodModal.hide()
  };

  public applyModal(): void {
      const text = this.newItem.trim();
      this.closeModal();
      if (text.length !== 0) {
        this.itemsService.createItem(text)
      }
  };

  ngOnDestroy() {
    this.subscription.unsubscribe();
  };
}
