import {Component, Input, Output, EventEmitter} from '@angular/core';

import { Item } from '../../classes/item'



@Component({
  selector: 'item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})

export class ItemComponent {
  @Input() item: Item;
  @Output() changeType = new EventEmitter();

  public changeCart() {
    this.changeType.emit( this.item.id )
  }

  public getClass(){
    const string = this.item.cart ? 'pull-left' : 'pull-right'
    return string
  }
}
