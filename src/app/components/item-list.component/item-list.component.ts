import {Component, OnInit, OnDestroy} from '@angular/core';
import {Item} from '../../classes/item';

import { Subscription } from 'rxjs/Subscription';

import {ItemsService} from '../../services/items.service'


@Component({
  selector: 'item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.css']
})

export class ItemListComponent implements OnInit, OnDestroy {
  items: Item[];
  subscription: Subscription;

  constructor(private itemsService: ItemsService) {
  };

  ngOnInit() {
    this.subscription = this.itemsService.getItems().subscribe(items => {
      this.items = items;
    });
  }

  public changeCart(id: number): void {
    this.itemsService.changeCart(id)
  };

  ngOnDestroy() {
    this.subscription.unsubscribe();
  };
}
