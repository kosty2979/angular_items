import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';


import { TooltipModule } from 'ngx-bootstrap/tooltip';
import {ConfirmationPopoverModule} from 'angular-confirmation-popover';
import { ModalModule } from 'ngx-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AutofocusDirective } from './directives/autofocus-directive';

import { ItemsService } from './services/items.service';

import { AppComponent } from './app.component';
import { ItemListComponent } from './components/item-list.component/item-list.component';
import { EditListComponent } from './components/edit-list.component/edit-list.component';
import { ItemComponent } from './components/item.component/item.component';
import { EditItemComponent } from './components/edit-item.component/edit-item.component';



@NgModule({
  declarations: [
    AutofocusDirective,
    AppComponent,
    ItemListComponent,
    EditListComponent,
    ItemComponent,
    EditItemComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    TooltipModule.forRoot(),
    ConfirmationPopoverModule.forRoot({
      confirmButtonType: 'danger'
    }),
    ModalModule.forRoot()
  ],
  providers: [
    ItemsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
